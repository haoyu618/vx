import fly from '../utils/request'
//二次封装，对接口进行封装成函数
/**
 * 
 * @param {object} data 
 * @param {object} options 
 */
export function getQingHua(data, options = {}) {
  return fly.request('https://api.uomg.com/api/rand.qinghua', data, options)
}