import Fly from '../lib/wx.js'
//第一次封装，添加请求拦截和响应拦截
const fly = new Fly()
fly.config.baseURL = " http: //129.211.169.131:13688 " //设置请求基础路径
fly.config.timeout = 10000 //设置请求超时时间

//请求拦截器
fly.interceptors.request.use(config => {
  // if (token) fly.config.headers.token = token
  wx.showLoading({
    title: '正在玩命加载。。。',
  })
  return config
}, err => {
  return Promise.reject(err)
})

//响应拦截
fly.interceptors.response.use(response => {
    // 根据后端接口返回的数据判断各种异常场景，比如token过期
    wx.showToast({
      title: '请求成功！！！',
    })

    return response.data
  },
  err => {
    //发生网络错误后会走到这里
    return Promise.resolve(err)
  }
)

export default fly