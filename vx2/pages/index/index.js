// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    canIUseGetUserProfile: false,
    canIUseOpenData: wx.canIUse('open-data.type.userAvatarUrl') && wx.canIUse('open-data.type.userNickName') // 如需尝试获取用户信息可改为false
  },



  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })

  },
  // 按钮跳转
  handleClick(){
    console.log(1);
    // 跳转到tabbar页面，关闭其他所有非tabbar页面
    // wx.switchTab({
    //   url: '/pages/logs/logs',
    // })

    // 关闭所有页面跳转到应该内的某个页面
    //  wx.reLaunch({
    //    url: '/pages/info/info',
    //  })

    // 关闭当前页面，跳转到应用内的某个页面
        //  wx.redirectTo({
        //    url: '/pages/logs/logs',
        //  })

    // 
    wx.navigateTo({
      url: '/pages/info/info',
    })
  },
  onLoad() {
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }

  },
  bindGetUserInfo(e){
    console.log(e.detail.userInfo);
  },
  getPhon(e){
    
    console.log(e);
  },
  getUserProfile(e) {
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认，开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '展示用户信息', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log(res)
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  getUserInfo(e) {
    // 不推荐使用getUserInfo获取用户信息，预计自2021年4月13日起，getUserInfo将不再弹出弹窗，并直接返回匿名的用户个人信息
    console.log(e)
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
